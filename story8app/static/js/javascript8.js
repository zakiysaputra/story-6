$(document).ready(function(){
    var search;
        
    $("#button-search").click(function(){
        search = $('#input-search').val();
        if(search === "" || search === null){
            displayError();
        }
        else {
            findBook();
        }
        return false;
    });
    
    
    function findBook(){
        $('#results').html("");
        console.log(search);    
        $.ajax({
            url: 'https://www.googleapis.com/books/v1/volumes?q=' + search,
            dataType: 'json',
            success: function(data){
                console.log(data);
                var items = data.items;
                for(var i = 0; i < items.length; i++){
                    let buku = items[i].volumeInfo;
                    
                    if ('title' in buku == false) var title = '<td class="text-center">-</td>';
                    else var title = '<td class="text-center">' + items[i].volumeInfo.title + '</td>';

                    if ('authors' in buku == false) var author = '<td class="text-center">-</td>';
                    else var author = '<td class="text-center">' + items[i].volumeInfo.authors + '</td>';

                    if ('publisher' in buku == false) var publisher = '<td class="text-center">-</td>';
                    else var publisher = '<td class="text-center">' + items[i].volumeInfo.publisher + '</td>';

                    if ('categories' in buku == false) var categori = '<td class="text-center">-</td>';
                    else var categori = '<td class="text-center">' + items[i].volumeInfo.categories + '</td>';

                    if ('imageLinks' in buku == false) var cover = '<td class="text-center">-</td>';
                    else var cover = '<td class="text-center"><img src="' + items[i].volumeInfo.imageLinks.smallThumbnail + '"></td>';
                    
                    $('#results').append('<tr>', title, author, publisher, categori, cover, '</tr>')
                }
            },
            type: 'GET'
        });
    }

});
