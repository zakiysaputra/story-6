from django.test import LiveServerTestCase, TestCase, SimpleTestCase
from django.utils import timezone


from .models import Status
from .forms import StatusForm
from .views import status


import time


class Story7Test(TestCase):
    # test url ada
    def test_landing_page_status_code(self):
        response = self.client.get('')
        self.assertEquals(response.status_code, 200)

    # test url invalid
    def test_invalid_url_not_found(self):
        response = self.client.get('no-this-is-patrick')
        self.assertEqual(response.status_code, 404)
    
    # cek landing make template
    def test_landing_page_use_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'index.html')
        
    # cek bisa bikin status
    def test_model_can_create_new_status(self):
        Status.objects.create(
            status='STAN LOONA',
            date=timezone.now()
        )
        count_all_status = Status.objects.all().count()
        self.assertEqual(count_all_status, 1)
       
    # cek isi html ada atau nggak
    def test_landing_page_contains_correct_html(self):
        response = self.client.get('')
        self.assertContains(response, 'recent status')
        
    def test_home_page_does_not_contain_incorrect_html(self):
        response = self.client.get('')
        self.assertNotContains(
            response, 'Hi there! I should not be on the page.')
    
    # cek auto date works atau nggak
    def test_mode_status_can_create_correct_date(self):
        response = self.client.post('', follow=True, data={
            'status': 'Lagi gabut',
        })

        today = timezone.now()
        status = Status.objects.first()

        self.assertEqual(status.date.date(), today.date())
