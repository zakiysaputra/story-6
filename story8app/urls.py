from django.urls import path
from . import views

app_name = 'story8app'

urlpatterns = [
    path('book/', views.book, name='book'),
]