
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

$('#heejinButton').click(switchheejin);
$('#hyunjinButton').click(switchhyunjin);
$('#haseulButton').click(switchhaseul);
$('#yeojinButton').click(switchyeojin);
$('#viviButton').click(switchvivi);
$('#kimlipButton').click(switchkimlip);
$('#jinsoulButton').click(switchjinsoul);
$('#choerryButton').click(switchchoerry);
$('#yvesButton').click(switchyves);
$('#chuuButton').click(switchchuu);
$('#gowonButton').click(switchgowon);
$('#oliviahyeButton').click(switcholiviahye);

function switchheejin() {
  $('body').attr('class', 'heejin');
}
function switchhyunjin() {
  $('body').attr('class', 'hyunjin');
}
function switchhaseul() {
  $('body').attr('class', 'haseul');
}
function switchyeojin() {
  $('body').attr('class', 'yeojin');
}
function switchvivi() {
  $('body').attr('class', 'vivi');
}
function switchkimlip() {
  $('body').attr('class', 'kimlip');
}
function switchjinsoul() {
  $('body').attr('class', 'jinsoul');
}
function switchchoerry() {
  $('body').attr('class', 'choerry');
}
function switchyves() {
  $('body').attr('class', 'yves');
}
function switchchuu() {
  $('body').attr('class', 'chuu');
}
function switchgowon() {
  $('body').attr('class', 'gowon');
}
function switcholiviahye() {
  $('body').attr('class', 'oliviahye');
}

