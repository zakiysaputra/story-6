from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from story7app import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('story7app.urls')),
    path('story8/', include('story8app.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
]
