from django.shortcuts import render
import json
from django.http import JsonResponse


# Create your views here.

def book(request):
    return render(request, 'book.html')
