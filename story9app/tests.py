from django.contrib.auth.models import User
from django.test import LiveServerTestCase, TestCase, SimpleTestCase
from django.urls import reverse


class Story9Test(TestCase):
    # test page login invalid
    def test_login_url_invalid(self):
        response = self.client.get('no-this-is-patrick')
        self.assertEqual(response.status_code, 404)
    
    # Test ada page login
    def test_login_user_exist(self):
        response = self.client.get('/')
        self.assertContains(response, "Hey there! don't forget to log in!")
        self.client.login(username='stanloona', password='loonaverse')

    # Test bisa login dengan pengguna yang udah dibikin di /admin/
    def test_login_user(self):
        self.client.login(username='stanloona', password='loonaverse')

    # Test gabisa login karena kesalahan input / pengguna tidak ada, serta pengguna merasa dia udah login
    def test_login_user_failed(self):
        self.client.login(username='stanloona', password='loonaverse')
        response = self.client.get('/')
        self.assertContains(response, "Hey there! don't forget to log in!")
    
    # Test bisa logout
    def test_logout_user(self):
        self.client.login(username='stanloona', password='loonaverse')
        self.client.get('/accounts/logout/', follow=True)
        response = self.client.get('/')
        self.assertContains(response, "Hey there! don't forget to log in!")
        
    