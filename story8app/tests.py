from django.test import LiveServerTestCase, TestCase, SimpleTestCase
from django.urls import reverse

class Story8Test(TestCase):
    # test url ada
    def test_page_status_code(self):
        response = self.client.get('/story8/book/')
        self.assertEquals(response.status_code, 200)
        
    # test ada atau nggak by name dan apakah templatenya sesuai
    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('story8app:book'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'book.html')

    # test url invalid
    def test_invalid_url_not_found(self):
        response = self.client.get('no-this-is-patrick')
        self.assertEqual(response.status_code, 404)
        
    # test contant element
    def test_page_contains_correct_html(self):
        response = self.client.get('/story8/book/')
        self.assertContains(response, 'Search the Book!')

    # test does not contain element
    def test_page_does_not_contain_incorrect_html(self):
        response = self.client.get('/story8/book/')
        self.assertNotContains(response, 'Hi there! I should not be on the page.')
        
