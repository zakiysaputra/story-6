from django.shortcuts import render, redirect

from .forms import StatusForm
from .models import Status

# Create your views here.

def status(request):
    form_class = StatusForm
    # if request is not post, initialize an empty form
    form = form_class(request.POST or None)
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('story_6:index')
            except:
                pass
        else:
            form = StatusForm()
    return render(request, "index.html", {'form': form, 'status': Status.objects.all()})