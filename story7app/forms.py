from django import forms
from .models import Status


class StatusForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class':'form-control'
            })

    def __init__(self, *args, **kwargs):
        super(StatusForm, self).__init__(*args, **kwargs)

        self.fields['status'].label = 'Status'
        self.fields['status'].widget = forms.Textarea(attrs={'class': 'form-control',
                                                             'placeholder': 'What are you thinking off?',
                                                             'rows': 5, 'cols': 7}
                                                      )
        self.fields['status'].error_messages = {'max_length': '300 character limit',
                                                'required': 'you can not be thinking of nothing right now!'}

    class Meta:
        model = Status
        fields = ['status']
